/*
 * Window theming support
 *
 * Copyright 2022 Zhiyi Zhang for CodeWeavers
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>

#include "windef.h"
#include "winbase.h"
#include "wingdi.h"
#include "winuser.h"
#include "ntuser.h"
#include "uxthemedll.h"
#include "vssym32.h"
#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(nonclient);

static const int caption_button_upper_right_spacing = 2; /* Pixels from the top and right side of the caption bar, excluding window frame */
static const int caption_button_lower_spacing = 3;       /* Pixels from the bottom side of the caption bar */
static const int caption_button_middle_spacing = 2;      /* Pixels between buttons */

static void uxtheme_draw_menu_button(HTHEME theme, HWND hwnd, HDC hdc, enum NONCLIENT_BUTTON_TYPE type,
                                     RECT rect, BOOL down, BOOL grayed)
{
    int part, state;

    switch (type)
    {
    case MENU_CLOSE_BUTTON:
        part = WP_MDICLOSEBUTTON;
        break;
    case MENU_MIN_BUTTON:
        part = WP_MDIMINBUTTON;
        break;
    case MENU_RESTORE_BUTTON:
        part = WP_MDIRESTOREBUTTON;
        break;
    case MENU_HELP_BUTTON:
        part = WP_MDIHELPBUTTON;
        break;
    /* There is no WP_MDIMAXBUTTON */
    default:
        user_api.pNonClientButtonDraw(hwnd, hdc, type, rect, down, grayed);
        return;
    }

    if (grayed)
        state = MINBS_DISABLED;
    else if (down)
        state = MINBS_PUSHED;
    else
        state = MINBS_NORMAL;

    if (IsThemeBackgroundPartiallyTransparent(theme, part, state))
        DrawThemeParentBackground(hwnd, hdc, &rect);
    DrawThemeBackground(theme, hdc, part, state, &rect, NULL);
}

static void uxtheme_draw_caption_min_button(HTHEME theme, HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed)
{
    int part, state;
    DWORD style;
    SIZE size;

    /* Tests on XP show that WP_CLOSEBUTTON is used instead of WP_MINBUTTON */
    GetThemePartSize(theme, hdc, WP_CLOSEBUTTON, 0, NULL, TS_DRAW, &size);
    style = GetWindowLongW(hwnd, GWL_STYLE);
    if (style & WS_SYSMENU)
        rect.right -= size.cx + caption_button_middle_spacing;
    if (style & (WS_MAXIMIZEBOX | WS_MINIMIZEBOX))
        rect.right -= size.cx + caption_button_middle_spacing;
    rect.right -= caption_button_upper_right_spacing;
    rect.left = rect.right - size.cx;
    rect.top += caption_button_upper_right_spacing;
    rect.bottom = rect.top + size.cy;

    part = (style & WS_MINIMIZE) ? WP_RESTOREBUTTON : WP_MINBUTTON;

    if (down)
        state = MINBS_PUSHED;
    else if (grayed)
        state = MINBS_DISABLED;
    else
        state = MINBS_NORMAL;

    if (IsThemeBackgroundPartiallyTransparent(theme, part, state))
        DrawThemeParentBackground(hwnd, hdc, &rect);
    DrawThemeBackground(theme, hdc, part, state, &rect, NULL);
}

static void uxtheme_draw_caption_max_button(HTHEME theme, HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed)
{
    int part, state;
    DWORD style;
    SIZE size;

    /* Tests on XP show that WP_CLOSEBUTTON is used instead of WP_MAXBUTTON */
    GetThemePartSize(theme, hdc, WP_CLOSEBUTTON, 0, NULL, TS_DRAW, &size);
    style = GetWindowLongW(hwnd, GWL_STYLE);
    if (style & WS_SYSMENU)
        rect.right -= size.cx + caption_button_middle_spacing;
    rect.right -= caption_button_upper_right_spacing;
    rect.left = rect.right - size.cx;
    rect.top += caption_button_upper_right_spacing;
    rect.bottom = rect.top + size.cy;

    part = (style & WS_MAXIMIZE) ? WP_RESTOREBUTTON : WP_MAXBUTTON;

    if (down)
        state = MAXBS_PUSHED;
    else if (grayed)
        state = MAXBS_DISABLED;
    else
        state = MAXBS_NORMAL;

    if (IsThemeBackgroundPartiallyTransparent(theme, part, state))
        DrawThemeParentBackground(hwnd, hdc, &rect);
    DrawThemeBackground(theme, hdc, part, state, &rect, NULL);
}

static void uxtheme_draw_caption_close_button(HTHEME theme, HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed)
{
    DWORD ex_style;
    SIZE size;
    int state;

    GetThemePartSize(theme, hdc, WP_CLOSEBUTTON, 0, NULL, TS_DRAW, &size);
    ex_style = GetWindowLongW(hwnd, GWL_EXSTYLE);

    /* A tool window has a smaller close button */
    if (ex_style & WS_EX_TOOLWINDOW)
    {
        /* Windows does not use SM_CXSMSIZE and SM_CYSMSIZE it uses 11x11 for the close button in tool window */
        const int bmp_height = 11;
        const int bmp_width = 11;
        int caption_height = GetSystemMetrics(SM_CYSMCAPTION);

        rect.top = rect.top + (caption_height - 1 - bmp_height) / 2;
        rect.left = rect.right - (caption_height + 1 + bmp_width) / 2;
        rect.bottom = rect.top + bmp_height;
        rect.right = rect.left + bmp_width;
    }
    else
    {
        rect.right -= caption_button_upper_right_spacing;
        rect.left = rect.right - size.cx;
        rect.top += caption_button_upper_right_spacing;
        rect.bottom = rect.top + size.cy;
    }

    if (down)
        state = CBS_PUSHED;
    else if (grayed)
        state = CBS_DISABLED;
    else
        state = CBS_NORMAL;

    if (IsThemeBackgroundPartiallyTransparent(theme, WP_CLOSEBUTTON, state))
        DrawThemeParentBackground(hwnd, hdc, &rect);
    DrawThemeBackground(theme, hdc, WP_CLOSEBUTTON, state, &rect, NULL);
}

void WINAPI UXTHEME_NonClientButtonDraw(HWND hwnd, HDC hdc, enum NONCLIENT_BUTTON_TYPE type,
                                        RECT rect, BOOL down, BOOL grayed)
{
    HTHEME theme;

    theme = OpenThemeDataForDpi(NULL, L"Window", GetDpiForWindow(hwnd));
    if (!theme)
    {
        user_api.pNonClientButtonDraw(hwnd, hdc, type, rect, down, grayed);
        return;
    }

    switch (type)
    {
    case MENU_CLOSE_BUTTON:
    case MENU_MIN_BUTTON:
    case MENU_MAX_BUTTON:
    case MENU_RESTORE_BUTTON:
    case MENU_HELP_BUTTON:
        uxtheme_draw_menu_button(theme, hwnd, hdc, type, rect, down, grayed);
        break;
    case CAPTION_MIN_BUTTON:
        uxtheme_draw_caption_min_button(theme, hwnd, hdc, rect, down, grayed);
        break;
    case CAPTION_MAX_BUTTON:
        uxtheme_draw_caption_max_button(theme, hwnd, hdc, rect, down, grayed);
        break;
    case CAPTION_CLOSE_BUTTON:
        uxtheme_draw_caption_close_button(theme, hwnd, hdc, rect, down, grayed);
        break;
    }

    CloseThemeData(theme);
}

/* The helper do not produce identical results from Windows, but it's close */
static int get_caption_size(HWND hwnd)
{
    return GetSystemMetricsForDpi(SM_CYSIZE, GetDpiForWindow(hwnd)) + caption_button_upper_right_spacing + caption_button_lower_spacing;
}

static BOOL has_big_frame(UINT style, UINT ex_style)
{
    return (style & (WS_THICKFRAME | WS_DLGFRAME)) || (ex_style & WS_EX_DLGMODALFRAME);
}

static BOOL has_static_outer_frame(UINT ex_style)
{
    return (ex_style & (WS_EX_STATICEDGE | WS_EX_DLGMODALFRAME)) == WS_EX_STATICEDGE;
}

static SIZE get_nc_frame_size(DWORD style, DWORD ex_style)
{
    SIZE size = {0};

    if (has_static_outer_frame(ex_style) || has_big_frame(style, ex_style))
    {
        size.cx = 2;
        size.cy = 2;
    }

    /* Firstly the "thick" frame */
    if (style & WS_THICKFRAME)
    {
        size.cx += GetSystemMetrics(SM_CXFRAME) - GetSystemMetrics(SM_CXDLGFRAME);
        size.cy += GetSystemMetrics(SM_CYFRAME) - GetSystemMetrics(SM_CYDLGFRAME);
    }

    /* Now the other bit of the frame */
    if ((style & (WS_BORDER | WS_DLGFRAME)) || (ex_style & WS_EX_DLGMODALFRAME))
    {
        size.cx += GetSystemMetrics(SM_CXDLGFRAME) - GetSystemMetrics(SM_CXEDGE);
        size.cy += GetSystemMetrics(SM_CYDLGFRAME) - GetSystemMetrics(SM_CYEDGE);
    }

    return size;
}

static BOOL uxtheme_draw_nc_sys_button(HWND hwnd, HDC hdc, RECT rect)
{
    HICON icon;
    POINT pt;

    icon = NtUserInternalGetWindowIcon(hwnd, ICON_BIG);
    if (!icon)
        icon = NtUserInternalGetWindowIcon(hwnd, ICON_SMALL);
    if (!icon)
        return FALSE;

    pt.x = rect.left + 2;
    pt.y = rect.top + (GetSystemMetrics(SM_CYCAPTION) - GetSystemMetrics(SM_CYSMICON)) / 2;
    DrawIconEx(hdc, pt.x, pt.y, icon, GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON),
               0, 0, DI_NORMAL);
    return TRUE;
}

static void draw_nc_frame(HTHEME theme, HWND hwnd, HDC hdc, RECT *rect, BOOL active, DWORD style,
                          DWORD ex_style)
{
    int state;
    SIZE size;
    RECT r;

    size = get_nc_frame_size(style, ex_style);
    state = active ? FS_ACTIVE : FS_INACTIVE;

    if (size.cx)
    {
        r = *rect;
        r.right = r.left + size.cx;
        if (IsThemeBackgroundPartiallyTransparent(theme, WP_FRAMELEFT, state))
            DrawThemeParentBackground(hwnd, hdc, &r);
        DrawThemeBackground(theme, hdc, WP_FRAMELEFT, state, &r, NULL);
        rect->left += size.cx;

        r = *rect;
        r.left = r.right - size.cx;
        if (IsThemeBackgroundPartiallyTransparent(theme, WP_FRAMERIGHT, state))
            DrawThemeParentBackground(hwnd, hdc, &r);
        DrawThemeBackground(theme, hdc, WP_FRAMERIGHT, state, &r, NULL);
        rect->right -= size.cx;
    }

    if (size.cy)
    {
        r.left = rect->left - size.cx;
        r.right = rect->right + size.cx;
        r.bottom = rect->bottom;
        r.top = rect->bottom - size.cy;
        if (IsThemeBackgroundPartiallyTransparent(theme, WP_FRAMEBOTTOM, state))
            DrawThemeParentBackground(hwnd, hdc, &r);
        DrawThemeBackground(theme, hdc, WP_FRAMEBOTTOM, state, &r, NULL);
        rect->bottom -= size.cy;
    }
}

static void draw_nc_caption(HTHEME theme, HWND hwnd, HDC hdc, RECT rect, RECT window_rect, DWORD style,
                            DWORD ex_style, BOOL active)
{
    WCHAR buffer[256];
    HMENU sys_menu;
    RECT r = rect;
    int state;

    state = active ? CS_ACTIVE : CS_INACTIVE;
    if (IsThemeBackgroundPartiallyTransparent(theme, WP_CAPTION, state))
        DrawThemeParentBackground(hwnd, hdc, &r);
    DrawThemeBackground(theme, hdc, WP_CAPTION, state, &r, NULL);

    if ((style & WS_SYSMENU) && !(ex_style & WS_EX_TOOLWINDOW))
    {
        if (uxtheme_draw_nc_sys_button(hwnd, hdc, window_rect))
            r.left += GetSystemMetrics(SM_CXSMICON) + 2;
    }

    if (style & WS_SYSMENU)
    {
        UINT menu_state;
        BOOL grayed;

        /* Go get the sysmenu */
        sys_menu = GetSystemMenu(hwnd, FALSE);
        menu_state = GetMenuState(sys_menu, SC_CLOSE, MF_BYCOMMAND);
        /* Draw a grayed close button if disabled or if SC_CLOSE is not there */
        grayed = (menu_state & (MF_DISABLED | MF_GRAYED)) || (menu_state == 0xFFFFFFFF);
        uxtheme_draw_caption_close_button(theme, hwnd, hdc, window_rect, FALSE, grayed);
        r.right -= GetSystemMetrics(SM_CYCAPTION) - 1;

        if ((style & WS_MAXIMIZEBOX) || (style & WS_MINIMIZEBOX))
        {
            uxtheme_draw_caption_max_button(theme, hwnd, hdc, window_rect, FALSE, !(style & WS_MAXIMIZEBOX));
            r.right -= GetSystemMetrics(SM_CXSIZE) + 1;

            uxtheme_draw_caption_min_button(theme, hwnd, hdc, window_rect, FALSE, !(style & WS_MINIMIZEBOX));
            r.right -= GetSystemMetrics(SM_CXSIZE) + 1;
        }
    }

    if (GetWindowTextW(hwnd, buffer, ARRAY_SIZE(buffer)))
    {
        NONCLIENTMETRICSW ncm;
        HFONT font, old_font;
        SIZE size;

        ncm.cbSize = sizeof(ncm);
        SystemParametersInfoW(SPI_GETNONCLIENTMETRICS, 0, &ncm, 0);
        if (ex_style & WS_EX_TOOLWINDOW)
            font = CreateFontIndirectW(&ncm.lfSmCaptionFont);
        else
            font = CreateFontIndirectW(&ncm.lfCaptionFont);
        old_font = SelectObject(hdc, font);
        if (active)
            SetTextColor(hdc, GetSysColor(COLOR_CAPTIONTEXT));
        else
            SetTextColor(hdc, GetSysColor(COLOR_INACTIVECAPTIONTEXT));
        SetBkMode(hdc, TRANSPARENT);

        size = get_nc_frame_size(style, ex_style);
        r.left += 2 + size.cx;
        DrawTextW(hdc, buffer, -1, &r, DT_SINGLELINE | DT_VCENTER | DT_NOPREFIX | DT_LEFT);
        DeleteObject(SelectObject(hdc, old_font));
    }
}

void WINAPI UXTHEME_NonClientFrameDraw(HWND hwnd, HDC hdc, RECT *rect, RECT window_rect,
                                       DWORD style, DWORD ex_style, BOOL active)
{
    RECT intersect, clip_rect;
    HTHEME theme;

    TRACE("hwnd %p hdc %p rect %s window_rect %s style %#lx ex_style %#lx active %d\n",
          hwnd, hdc, wine_dbgstr_rect(rect), wine_dbgstr_rect(&window_rect), style, ex_style, active);

    theme = OpenThemeDataForDpi(NULL, L"Window", GetDpiForWindow(hwnd));
    if (!theme)
    {
        user_api.pNonClientFrameDraw(hwnd, hdc, rect, window_rect, style, ex_style, active);
        return;
    }

    if ((style & WS_CAPTION) == WS_CAPTION)
    {
        RECT r = *rect;
        int height;

        if (ex_style & WS_EX_TOOLWINDOW)
        {
            SIZE size;

            GetThemePartSize(theme, hdc, WP_SMALLCAPTION, 0, NULL, TS_DRAW, &size);
            height = size.cy;
        }
        else
        {
            height = get_caption_size(hwnd);
        }

        r.bottom = rect->top + height;
        rect->top += height;

        GetClipBox(hdc, &clip_rect);
        if (IntersectRect(&intersect, &r, &clip_rect))
            draw_nc_caption(theme, hwnd, hdc, r, window_rect, style, ex_style, active);
    }

    draw_nc_frame(theme, hwnd, hdc, rect, active, style, ex_style);
    CloseThemeData(theme);
}
