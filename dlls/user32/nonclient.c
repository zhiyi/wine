/*
 * Non-client area window functions
 *
 * Copyright 1994 Alexandre Julliard
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include "user_private.h"
#include "controls.h"
#include "wine/debug.h"


#define SC_ABOUTWINE            (SC_SCREENSAVE+1)

/***********************************************************************
 *		DrawCaption (USER32.@) Draws a caption bar
 */
BOOL WINAPI DrawCaption( HWND hwnd, HDC hdc, const RECT *rect, UINT flags )
{
    return NtUserDrawCaptionTemp( hwnd, hdc, rect, 0, 0, NULL, flags & 0x103f );
}


/***********************************************************************
 *		DrawCaptionTempA (USER32.@)
 */
BOOL WINAPI DrawCaptionTempA (HWND hwnd, HDC hdc, const RECT *rect, HFONT hFont,
                              HICON hIcon, LPCSTR str, UINT uFlags)
{
    LPWSTR strW;
    INT len;
    BOOL ret = FALSE;

    if (!(uFlags & DC_TEXT) || !str)
        return NtUserDrawCaptionTemp( hwnd, hdc, rect, hFont, hIcon, NULL, uFlags );

    len = MultiByteToWideChar( CP_ACP, 0, str, -1, NULL, 0 );
    if ((strW = HeapAlloc( GetProcessHeap(), 0, len * sizeof(WCHAR) )))
    {
        MultiByteToWideChar( CP_ACP, 0, str, -1, strW, len );
        ret = NtUserDrawCaptionTemp( hwnd, hdc, rect, hFont, hIcon, strW, uFlags );
        HeapFree( GetProcessHeap (), 0, strW );
    }
    return ret;
}


/***********************************************************************
 *           NC_HandleSysCommand
 *
 * Handle a WM_SYSCOMMAND message. Called from DefWindowProc().
 */
LRESULT NC_HandleSysCommand( HWND hwnd, WPARAM wParam, LPARAM lParam )
{
    if (!NtUserMessageCall( hwnd, WM_SYSCOMMAND, wParam, lParam, 0, NtUserDefWindowProc, FALSE ))
        return 0;

    switch (wParam & 0xfff0)
    {
    case SC_TASKLIST:
        WinExec( "taskman.exe", SW_SHOWNORMAL );
        break;

    case SC_SCREENSAVE:
        if (wParam == SC_ABOUTWINE)
        {
            HMODULE hmodule = LoadLibraryA( "shell32.dll" );
            if (hmodule)
            {
                BOOL (WINAPI *aboutproc)(HWND, LPCSTR, LPCSTR, HICON);
                const char * (CDECL *p_wine_get_version)(void);
                char app[256];

                p_wine_get_version = (void *)GetProcAddress( GetModuleHandleW(L"ntdll.dll"), "wine_get_version" );
                aboutproc = (void *)GetProcAddress( hmodule, "ShellAboutA" );
                if (p_wine_get_version && aboutproc)
                {
                    snprintf( app, ARRAY_SIZE(app), "Wine %s", p_wine_get_version() );
                    aboutproc( hwnd, app, NULL, 0 );
                }
                FreeLibrary( hmodule );
            }
        }
        break;
    }
    return 0;
}

static void user_draw_mdi_button( HDC hdc, enum NONCLIENT_BUTTON_TYPE type, RECT rect, BOOL down, BOOL grayed )
{
    UINT flags;

    switch (type)
    {
    case MENU_CLOSE_BUTTON:
        flags = DFCS_CAPTIONCLOSE;
        break;
    case MENU_MIN_BUTTON:
        flags = DFCS_CAPTIONMIN;
        break;
    case MENU_MAX_BUTTON:
        flags = DFCS_CAPTIONMAX;
        break;
    case MENU_RESTORE_BUTTON:
        flags = DFCS_CAPTIONRESTORE;
        break;
    case MENU_HELP_BUTTON:
        flags = DFCS_CAPTIONHELP;
        break;
    default:
        return;
    }

    if (down)
        flags |= DFCS_PUSHED;
    if (grayed)
        flags |= DFCS_INACTIVE;

    DrawFrameControl( hdc, &rect, DFC_CAPTION, flags );
}

static void user_draw_caption_min_button( HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed )
{
    DWORD style, ex_style;
    UINT flags;

    /* never draw minimize box when window has WS_EX_TOOLWINDOW style */
    ex_style = GetWindowLongW( hwnd, GWL_EXSTYLE );
    if (ex_style & WS_EX_TOOLWINDOW) return;

    style = GetWindowLongW( hwnd, GWL_STYLE );
    flags = (style & WS_MINIMIZE) ? DFCS_CAPTIONRESTORE : DFCS_CAPTIONMIN;

    if (style & WS_SYSMENU)
        rect.right -= GetSystemMetrics( SM_CXSIZE );
    if (style & (WS_MAXIMIZEBOX | WS_MINIMIZEBOX))
        rect.right -= GetSystemMetrics( SM_CXSIZE ) - 2;
    rect.left = rect.right - GetSystemMetrics( SM_CXSIZE );
    rect.bottom = rect.top + GetSystemMetrics( SM_CYSIZE ) - 2;
    rect.top += 2;
    rect.right -= 2;
    if (down) flags |= DFCS_PUSHED;
    if (grayed) flags |= DFCS_INACTIVE;
    DrawFrameControl( hdc, &rect, DFC_CAPTION, flags );
}

static void user_draw_caption_max_button( HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed )
{
    DWORD style, ex_style;
    UINT flags;

    /* never draw maximize box when window has WS_EX_TOOLWINDOW style */
    ex_style = GetWindowLongW( hwnd, GWL_EXSTYLE );
    if (ex_style & WS_EX_TOOLWINDOW) return;

    style = GetWindowLongW( hwnd, GWL_STYLE );
    flags = (style & WS_MAXIMIZE) ? DFCS_CAPTIONRESTORE : DFCS_CAPTIONMAX;

    if (style & WS_SYSMENU) rect.right -= GetSystemMetrics( SM_CXSIZE );
    rect.left = rect.right - GetSystemMetrics( SM_CXSIZE );
    rect.bottom = rect.top + GetSystemMetrics( SM_CYSIZE ) - 2;
    rect.top += 2;
    rect.right -= 2;
    if (down) flags |= DFCS_PUSHED;
    if (grayed) flags |= DFCS_INACTIVE;
    DrawFrameControl( hdc, &rect, DFC_CAPTION, flags );
}

static void user_draw_caption_close_button( HWND hwnd, HDC hdc, RECT rect, BOOL down, BOOL grayed )
{
    DWORD ex_style = GetWindowLongW( hwnd, GWL_EXSTYLE );
    UINT flags = DFCS_CAPTIONCLOSE;

    /* A tool window has a smaller close button */
    if (ex_style & WS_EX_TOOLWINDOW)
    {
        /* Windows does not use SM_CXSMSIZE and SM_CYSMSIZE
         * it uses 11x11 for the close button in tool window */
        int bmp_height = MulDiv( 11, GetDpiForWindow( hwnd ), 96 );
        int bmp_width = bmp_height;
        int caption_height = GetSystemMetrics( SM_CYSMCAPTION );

        rect.top = rect.top + (caption_height - 1 - bmp_height) / 2;
        rect.left = rect.right - (caption_height + 1 + bmp_width) / 2;
        rect.bottom = rect.top + bmp_height;
        rect.right = rect.left + bmp_width;
    }
    else
    {
        rect.left = rect.right - GetSystemMetrics( SM_CXSIZE );
        rect.bottom = rect.top + GetSystemMetrics( SM_CYSIZE ) - 2;
        rect.top += 2;
        rect.right -= 2;
    }

    if (down) flags |= DFCS_PUSHED;
    if (grayed) flags |= DFCS_INACTIVE;
    DrawFrameControl( hdc, &rect, DFC_CAPTION, flags );
}

void WINAPI USER_NonClientButtonDraw( HWND hwnd, HDC hdc, enum NONCLIENT_BUTTON_TYPE type,
                                      RECT rect, BOOL down, BOOL grayed )
{
    switch (type)
    {
    case MENU_CLOSE_BUTTON:
    case MENU_MIN_BUTTON:
    case MENU_MAX_BUTTON:
    case MENU_RESTORE_BUTTON:
    case MENU_HELP_BUTTON:
        user_draw_mdi_button( hdc, type, rect, down, grayed );
        return;
    case CAPTION_MIN_BUTTON:
        user_draw_caption_min_button( hwnd, hdc, rect, down, grayed );
        return;
    case CAPTION_MAX_BUTTON:
        user_draw_caption_max_button( hwnd, hdc, rect, down, grayed );
        return;
    case CAPTION_CLOSE_BUTTON:
        user_draw_caption_close_button( hwnd, hdc, rect, down, grayed );
        return;
    }
}

static BOOL user_draw_nc_sys_button( HWND hwnd, HDC hdc, RECT rect )
{
    HICON icon;
    POINT pt;

    icon = NtUserInternalGetWindowIcon( hwnd, ICON_BIG );
    if (!icon)
        icon = NtUserInternalGetWindowIcon( hwnd, ICON_SMALL );
    if (!icon)
        return FALSE;

    pt.x = rect.left + 2;
    pt.y = rect.top + (GetSystemMetrics( SM_CYCAPTION ) - GetSystemMetrics( SM_CYSMICON )) / 2;
    NtUserDrawIconEx( hdc, pt.x, pt.y, icon, GetSystemMetrics( SM_CXSMICON ),
                      GetSystemMetrics( SM_CYSMICON ), 0, 0, DI_NORMAL);
    return TRUE;
}

/* Draws the bar part(i.e. the big rectangle) of the caption */
static void user_draw_nc_caption_bar( HDC hdc, const RECT *rect, DWORD style, BOOL active,
                                      BOOL gradient )
{
    if (gradient)
    {
        static GRADIENT_RECT mesh[] = {{0, 1}, {1, 2}, {2, 3}};
        int button_size = GetSystemMetrics( SM_CYCAPTION ) - 1;
        TRIVERTEX vertices[4];
        DWORD left, right;

        left = GetSysColor( active ? COLOR_ACTIVECAPTION : COLOR_INACTIVECAPTION );
        right = GetSysColor( active ? COLOR_GRADIENTACTIVECAPTION : COLOR_GRADIENTINACTIVECAPTION );
        vertices[0].Red   = vertices[1].Red   = GetRValue (left) << 8;
        vertices[0].Green = vertices[1].Green = GetGValue (left) << 8;
        vertices[0].Blue  = vertices[1].Blue  = GetBValue (left) << 8;
        vertices[0].Alpha = vertices[1].Alpha = 0xff00;
        vertices[2].Red   = vertices[3].Red   = GetRValue (right) << 8;
        vertices[2].Green = vertices[3].Green = GetGValue (right) << 8;
        vertices[2].Blue  = vertices[3].Blue  = GetBValue (right) << 8;
        vertices[2].Alpha = vertices[3].Alpha = 0xff00;

        if ((style & WS_SYSMENU) && ((style & WS_MAXIMIZEBOX) || (style & WS_MINIMIZEBOX)))
            button_size += 2 * (GetSystemMetrics(SM_CXSIZE) + 1);

        /* area behind icon; solid filled with left color */
        vertices[0].x = rect->left;
        vertices[0].y = rect->top;
        if (style & WS_SYSMENU)
            vertices[1].x = min(rect->left + GetSystemMetrics( SM_CXSMICON ), rect->right);
        else
            vertices[1].x = vertices[0].x;
        vertices[1].y = rect->bottom;

        /* area behind text; gradient */
        vertices[2].x = max(vertices[1].x, rect->right - button_size);
        vertices[2].y = rect->top;

        /* area behind buttons; solid filled with right color */
        vertices[3].x = rect->right;
        vertices[3].y = rect->bottom;

        GdiGradientFill( hdc, vertices, 4, mesh, 3, GRADIENT_FILL_RECT_H );
    }
    else
    {
        INT color = active ? COLOR_ACTIVECAPTION : COLOR_INACTIVECAPTION;
        FillRect( hdc, rect, GetSysColorBrush( color ) );
    }
}

static BOOL has_big_frame( UINT style, UINT ex_style )
{
    return (style & (WS_THICKFRAME | WS_DLGFRAME)) || (ex_style & WS_EX_DLGMODALFRAME);
}

static BOOL has_static_outer_frame( UINT ex_style )
{
    return (ex_style & (WS_EX_STATICEDGE | WS_EX_DLGMODALFRAME)) == WS_EX_STATICEDGE;
}

/* Draw a window frame inside the given rectangle, and update the rectangle. */
static void draw_nc_frame( HDC hdc, RECT  *rect, BOOL  active, DWORD style, DWORD ex_style )
{
    INT width, height;

    if (has_static_outer_frame( ex_style ))
        DrawEdge( hdc, rect, BDR_SUNKENOUTER, BF_RECT | BF_ADJUST );
    else if (has_big_frame( style, ex_style ))
        DrawEdge( hdc, rect, EDGE_RAISED, BF_RECT | BF_ADJUST);

    /* Firstly the "thick" frame */
    if (style & WS_THICKFRAME)
    {
        width = GetSystemMetrics( SM_CXFRAME ) - GetSystemMetrics( SM_CXDLGFRAME );
        height = GetSystemMetrics( SM_CYFRAME ) - GetSystemMetrics( SM_CYDLGFRAME );

        SelectObject( hdc, GetSysColorBrush( active ? COLOR_ACTIVEBORDER : COLOR_INACTIVEBORDER ) );
        /* Draw frame */
        PatBlt( hdc, rect->left, rect->top, rect->right - rect->left, height, PATCOPY );
        PatBlt( hdc, rect->left, rect->top, width, rect->bottom - rect->top, PATCOPY );
        PatBlt( hdc, rect->left, rect->bottom - 1, rect->right - rect->left, -height, PATCOPY );
        PatBlt( hdc, rect->right - 1, rect->top, -width, rect->bottom - rect->top, PATCOPY );

        InflateRect( rect, -width, -height );
    }

    /* Now the other bit of the frame */
    if ((style & (WS_BORDER | WS_DLGFRAME)) || (ex_style & WS_EX_DLGMODALFRAME))
    {
        INT color;

        width = GetSystemMetrics( SM_CXDLGFRAME ) - GetSystemMetrics( SM_CXEDGE );
        height = GetSystemMetrics( SM_CYDLGFRAME ) - GetSystemMetrics( SM_CYEDGE );
        /* This should give a value of 1 that should also work for a border */

        if (ex_style & (WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE)) color = COLOR_3DFACE;
        else if (ex_style & WS_EX_STATICEDGE) color = COLOR_WINDOWFRAME;
        else if (style & (WS_DLGFRAME|WS_THICKFRAME)) color = COLOR_3DFACE;
        else color = COLOR_WINDOWFRAME;
        SelectObject( hdc, GetSysColorBrush( color ));

        /* Draw frame */
        PatBlt( hdc, rect->left, rect->top, rect->right - rect->left, height, PATCOPY );
        PatBlt( hdc, rect->left, rect->top, width, rect->bottom - rect->top, PATCOPY );
        PatBlt( hdc, rect->left, rect->bottom - 1, rect->right - rect->left, -height, PATCOPY );
        PatBlt( hdc, rect->right - 1, rect->top, -width, rect->bottom - rect->top, PATCOPY );

        InflateRect( rect, -width, -height );
    }
}

static void draw_nc_caption( HWND hwnd, HDC hdc, RECT rect, RECT window_rect, DWORD style,
                             DWORD ex_style, BOOL active )
{
    INT pen_color = COLOR_3DFACE;
    BOOL gradient = FALSE;
    WCHAR buffer[256];
    HMENU sys_menu;
    HPEN prev_pen;
    RECT r = rect;

    if ((ex_style & (WS_EX_STATICEDGE | WS_EX_CLIENTEDGE | WS_EX_DLGMODALFRAME)) == WS_EX_STATICEDGE)
        pen_color = COLOR_WINDOWFRAME;
    prev_pen = SelectObject( hdc, SYSCOLOR_GetPen( pen_color ) );
    MoveToEx( hdc, r.left, r.bottom - 1, NULL );
    LineTo( hdc, r.right, r.bottom - 1 );
    SelectObject( hdc, prev_pen );
    r.bottom--;

    SystemParametersInfoW( SPI_GETGRADIENTCAPTIONS, 0, &gradient, 0 );
    user_draw_nc_caption_bar( hdc, &r, style, active, gradient );

    if ((style & WS_SYSMENU) && !(ex_style & WS_EX_TOOLWINDOW))
    {
        if (user_draw_nc_sys_button( hwnd, hdc, window_rect ))
            r.left += GetSystemMetrics( SM_CXSMICON ) + 2;
    }

    if (style & WS_SYSMENU)
    {
        BOOL grayed;
        UINT state;

        /* Go get the sysmenu */
        sys_menu = NtUserGetSystemMenu( hwnd, FALSE );
        state = GetMenuState( sys_menu, SC_CLOSE, MF_BYCOMMAND );

        /* Draw a grayed close button if disabled or if SC_CLOSE is not there */
        grayed = (state & (MF_DISABLED | MF_GRAYED)) || (state == 0xFFFFFFFF);
        user_draw_caption_close_button( hwnd, hdc, window_rect, FALSE, grayed);
        r.right -= GetSystemMetrics( SM_CYCAPTION ) - 1;

        if ((style & WS_MAXIMIZEBOX) || (style & WS_MINIMIZEBOX))
        {
            user_draw_caption_max_button( hwnd, hdc, window_rect, FALSE, !(style & WS_MAXIMIZEBOX));
            r.right -= GetSystemMetrics( SM_CXSIZE ) + 1;

            user_draw_caption_min_button( hwnd, hdc, window_rect, FALSE, !(style & WS_MINIMIZEBOX));
            r.right -= GetSystemMetrics( SM_CXSIZE ) + 1;
        }
    }

    if (GetWindowTextW( hwnd, buffer, ARRAY_SIZE(buffer) ))
    {
        NONCLIENTMETRICSW ncm;
        HFONT font, old_font;

        ncm.cbSize = sizeof(ncm);
        SystemParametersInfoW( SPI_GETNONCLIENTMETRICS, 0, &ncm, 0 );
        if (ex_style & WS_EX_TOOLWINDOW)
            font = CreateFontIndirectW( &ncm.lfSmCaptionFont );
        else
            font = CreateFontIndirectW( &ncm.lfCaptionFont );
        old_font = SelectObject( hdc, font );
        if (active)
            SetTextColor( hdc, GetSysColor( COLOR_CAPTIONTEXT ) );
        else
            SetTextColor( hdc, GetSysColor( COLOR_INACTIVECAPTIONTEXT ) );
        SetBkMode( hdc, TRANSPARENT );
        r.left += 2;
        DrawTextW( hdc, buffer, -1, &r, DT_SINGLELINE | DT_VCENTER | DT_NOPREFIX | DT_LEFT );
        DeleteObject( SelectObject( hdc, old_font ) );
    }
}

void WINAPI USER_NonClientFrameDraw( HWND hwnd, HDC hdc, RECT *rect, RECT window_rect,
                                     DWORD style, DWORD ex_style, BOOL active )
{
    RECT intersect_rect, clip_rect;

    SelectObject( hdc, SYSCOLOR_GetPen( COLOR_WINDOWFRAME ) );

    GetClipBox( hdc, &clip_rect );

    draw_nc_frame( hdc, rect, active, style, ex_style );

    if ((style & WS_CAPTION) == WS_CAPTION)
    {
        RECT r = *rect;

        if (ex_style & WS_EX_TOOLWINDOW)
        {
            r.bottom = rect->top + GetSystemMetrics( SM_CYSMCAPTION );
            rect->top += GetSystemMetrics( SM_CYSMCAPTION );
        }
        else
        {
            r.bottom = rect->top + GetSystemMetrics( SM_CYCAPTION );
            rect->top += GetSystemMetrics( SM_CYCAPTION );
        }

        if (IntersectRect( &intersect_rect, &r, &clip_rect ))
            draw_nc_caption( hwnd, hdc, r, window_rect, style, ex_style, active);
    }
}
